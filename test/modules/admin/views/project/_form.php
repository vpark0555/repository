<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'price')->textInput() ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'start_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Start date...'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>

            <?= $form->field($model, 'end_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'End date...'],
                'pluginOptions' => [
                    'autoclose' => true
                ]
            ]); ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

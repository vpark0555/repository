<?php

use app\models\Language;
use kartik\typeahead\TypeaheadBasic;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\WordSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="word-search">
    <?php Pjax::begin(['id' => 'wordSearch']) ?>
    <?php $form = ActiveForm::begin([
        'options' => ['data-pjax' => true],
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-md-8">
        <?= $form->field($model, 'word') ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'language_id')->dropDownList(ArrayHelper::map(Language::find()->all(), 'id', 'language'), ['prompt' => 'Все']) ?>
    </div>


    <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>

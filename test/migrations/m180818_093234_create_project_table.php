<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180818_093234_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45),
            'price' => $this->double(),
            'start_date'=> $this->string(),
            'end_date' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}

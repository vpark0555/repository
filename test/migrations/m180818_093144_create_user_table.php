<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180818_093144_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45),
            'username' => $this->string(45),
            'password' => $this->string(255),
            'auth_key' => $this->string(255)
        ]);

        $this->insert('user', [
            'name' => 'admin',
            'username' => 'admin',
            'password' => \Yii::$app->security->generatePasswordHash('admin', 10),
            'auth_key' => '123'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}

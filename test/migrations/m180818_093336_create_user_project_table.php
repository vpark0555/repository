<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_project`.
 */
class m180818_093336_create_user_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user_project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'project_id' => $this->integer()
        ]);

        $this->createIndex(
            'user_project_user_id',
            'user_project',
            'user_id'
        );

        $this->addForeignKey(
            'user_project_user_id',
            'user_project',
            'user_id',
            'user',
            'id',
            'RESTRICT'
        );

        $this->createIndex(
            'user_project_project_id',
            'user_project',
            'project_id'
        );

        $this->addForeignKey(
            'user_project_project_id',
            'user_project',
            'project_id',
            'project',
            'id',
            'RESTRICT'
        );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user_project');
    }
}

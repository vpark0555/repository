<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $name
 * @property double $price
 * @property string $start_date
 * @property string $end_date
 *
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['name'], 'string', 'max' => 45],
            [['start_date', 'end_date'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_project', ['project_id' => 'id']);
    }

    public function getAssignedUsers()
    {
        $assignedUsers = $this->getUsers()->select('id')->asArray()->all();
        return ArrayHelper::getColumn($assignedUsers, 'id');
    }

    public function saveUsers($users)
    {
        if (is_array($users)) {
            $this->clearCurrentAssignment();
            foreach ($users as $user_id) {
                $user = User::findOne($user_id);
                $this->link('users', $user);
            }
        }
    }

    public function clearCurrentAssignment()
    {
        UserProject::deleteAll();
    }
}
